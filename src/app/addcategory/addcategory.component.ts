import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-addcategory',
  templateUrl: './addcategory.component.html',
  styleUrls: ['./addcategory.component.css']
})
export class AddcategoryComponent implements OnInit {

  categories: any;

  constructor(private service: UserService, private snack: MatSnackBar) {
    
    this.categories = { cid: '', title: '', description: '' };
  }
  ngOnInit(): void {
  }

  formSubmit(addCat: any) {

    this.categories.title = addCat.title;
    this.categories.description = addCat.description;

    console.log(this.categories);

    if (this.categories.title.trim() == '' || this.categories.title == null) {
      this.snack.open("Title Required!!", '', { duration: 3000 });
      return;
    }

    this.service.addCategory(this.categories).subscribe(
      (data: any) => {
        console.log(data);
        this.snack.open("Category Successfully added", '', { duration: 3000 });
      });
  }

}
