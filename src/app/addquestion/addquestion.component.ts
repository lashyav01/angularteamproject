import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { UserService } from '../user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-addquestion',
  templateUrl: './addquestion.component.html',
  styleUrls: ['./addquestion.component.css']
})
export class AddquestionComponent implements OnInit{

  public Editor=ClassicEditor;
  qId:any;
  qTitle:any;
  question:any
 
  constructor(private route:ActivatedRoute,private snack:MatSnackBar,private service:UserService)
  {
    this.question={ quizzes:{ qid:''},
    content:'',
    option1:'',
    option2:'',
    option3:'',
    option4:'',
    ans:''   };

  }
  ngOnInit()
  {
    this.qId=this.route.snapshot.params['qid'];
    this.qTitle=this.route.snapshot.params['title'];
    this.question.quizzes.qid=this.qId;
    
  }
  formSubmit()
  {
    if(this.question.content.trim()==''||this.question.content==null)
    {
      this.snack.open("Question is required",'',{duration:3000});
      return;
    }
    if(this.question.option1.trim()==''||this.question.option1==null)
    {
      this.snack.open("Minimum Two options are required",'',{duration:3000});
      return;
    }
    if(this.question.option2.trim()==''||this.question.option2==null)
    {
      this.snack.open("Minimum Two options are required",'',{duration:3000});
      return;
    }

    this.service.addQuestion(this.question).subscribe((data: any) => {
      console.log(data);
        Swal.fire('Success','Question added','success');
      },
      (error)=>
      {
        Swal.fire('Error','Unable to Add Question','error');
      });
    }
    
}
