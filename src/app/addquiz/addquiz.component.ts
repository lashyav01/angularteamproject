import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-addquiz',
  templateUrl: './addquiz.component.html',
  styleUrls: ['./addquiz.component.css']
})
export class AddquizComponent implements OnInit {

  categories: any;
  categoryData: any;

  constructor(private service: UserService, private snack: MatSnackBar) {

    this.categories = [{ cid: '', title: '', description: '' }];
    this.categoryData = {
      title: '',
      description: '',
      maxMarks: '',
      noOfQuestions: '',
      active: true,
      category: { cid: '' }
    }
  }

  ngOnInit(): void {

    this.service.viewcategories().subscribe((data: any) => {
      this.categories = data;
      console.log(this.categories);
    },
      (Error) => {
        console.log(Error);
        Swal.fire('error!', 'Error', 'error');

      });
  }

  addQuiz() {

    //console.log(this.categoryData);
    if (this.categoryData.title.trim() == '' || this.categoryData.title == null) {
      this.snack.open("Title is required", '', { duration: 3000 });
      return;
    }

    this.service.addQuiz(this.categoryData).subscribe(
      (data: any) => {
        Swal.fire('Success', 'quiz is added', 'success')
        this.categoryData =
        {
          title: '',
          description: '',
          maxMarks: '',
          noOfQuestions: '',
          active: true,
          category:{ cid: ''}
        }
      },
      (Error) => {
        Swal.fire('Error!!', 'Error while adding the quiz', 'error');
      });
  }

}
