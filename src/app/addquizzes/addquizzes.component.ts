import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import Swal from 'sweetalert2';
import { MatSnackBar } from '@angular/material/snack-bar';
import { QuizService } from '../quiz.service';
@Component({
  selector: 'app-addquizzes',
  templateUrl: './addquizzes.component.html',
  styleUrls: ['./addquizzes.component.css']
})
export class AddquizzesComponent implements OnInit{
  categories=[{cid:'',title:'',description:''}];
  categoryData=
  {
    title:'',
    description:'',
    maxMarks:'',
    noOfQuestions:'',
    active:true,
    category:
    {
      cid:''
    }

  }
  constructor(private service:CategoryService,private snack:MatSnackBar,private quizs:QuizService)
  {

  }
  ngOnInit(): void {
    this.service.categories().subscribe((data:any)=>
    {
      this.categories=data;
      console.log(this.categories);
    },
    (Error)=>
    {
      console.log(Error);
      Swal.fire('error!','Error','error');
    }
    ) 
  }
  addQuiz()
  {
    //console.log(this.categoryData);
    if(this.categoryData.title.trim()==''||this.categoryData.title==null)
    {
      this.snack.open("Title is required",'',{duration:3000});
      return;
    }
    this.quizs.addQuiz(this.categoryData).subscribe(
      (data:any)=>
      {
        Swal.fire('Success','quiz is added','success')
        this.categoryData=
        {
          title:'',
          description:'',
          maxMarks:'',
          noOfQuestions:'',
          active:true,
          category:
          {
            cid:''
          }
      
        }
      },
      (Error)=>
      {
        Swal.fire('Error!!','Error while adding the quiz','error');
        
      }

    )
  }

}
