import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';
import { InstructionsComponent } from './instructions/instructions.component';
import { LoadquizComponent } from './loadquiz/loadquiz.component';
import { ViewcategoriesComponent } from './viewcategories/viewcategories.component';
import { AddcategoryComponent } from './addcategory/addcategory.component';
import { ViewquizzesComponent } from './viewquizzes/viewquizzes.component';
import { AddquizComponent } from './addquiz/addquiz.component';
import { UpdatequizComponent } from './updatequiz/updatequiz.component';
import { ViewquestionsComponent } from './viewquestions/viewquestions.component';
import { AddquestionComponent } from './addquestion/addquestion.component';
import { HomepageComponent } from './homepage/homepage.component';
import { StartComponent } from './start/start.component';
import { GoogleauthComponent } from './googleauth/googleauth.component';
import { AuthGuard } from './auth.guard';
import { LogoutComponent } from './logout/logout.component';
import { UploadQuestionComponent } from './upload-question/upload-question.component';

const routes: Routes = [
  {path:"", component:HomepageComponent},
  {path:"register", component:RegisterComponent},
  {path:"login", component:LoginComponent},
  {path:"profile", component:ProfileComponent},
  {path:"logout", canActivate:[AuthGuard], component:LogoutComponent},

  { path:"userdashboard",canActivate:[AuthGuard], component:UserdashboardComponent,
    children:[
      {path:":catid",component:LoadquizComponent},
      
  ]
},
{path:"instructions/:qid", canActivate:[AuthGuard], component:InstructionsComponent},
{path:"start/:qid", canActivate:[AuthGuard],component:StartComponent},



  {path:"admin", canActivate:[AuthGuard], component:AdmindashboardComponent,
    children:[
    {path:"profile",component:ProfileComponent},
    {path:"home",component:HomepageComponent},
    {path:"viewcat",component:ViewcategoriesComponent},
    {path:"addcat",component:AddcategoryComponent},
    {path:"viewquiz",component:ViewquizzesComponent},
    {path:"addquiz",component:AddquizComponent},
    {path:"updatequiz/:qid",component:UpdatequizComponent},
    {path:"viewquestions/:qid/:title",component:ViewquestionsComponent},
    {path:'question/:qid/:title',component:AddquestionComponent},
    {path:'uploadquestions/:qid',component:UploadQuestionComponent}

  ]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule  { }
