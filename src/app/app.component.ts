import { Component,OnInit, ViewChild, ElementRef  } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'quizz-app';
  // auth2: any;
    
  // @ViewChild('loginRef', {static: true }) loginElement!: ElementRef;
     
  // constructor() { }
    
  // /*------------------------------------------
  // --------------------------------------------
  // About 
  // --------------------------------------------
  // --------------------------------------------*/
  // ngOnInit() {
     
  //   this.googleAuthSDK();
  // }
    
  // /**
  //  * Write code on Method
  //  *
  //  * @return response()
  //  */
  // callLoginButton() {
     
  //   this.auth2.attachClickHandler(this.loginElement.nativeElement, {},
  //     (googleAuthUser:any) => {
     
  //       let profile = googleAuthUser.getBasicProfile();
  //       console.log('Token || ' + googleAuthUser.getAuthResponse().id_token);
  //       console.log('ID: ' + profile.getId());
  //       console.log('Name: ' + profile.getName());
  //       console.log('Image URL: ' + profile.getImageUrl());
  //       console.log('Email: ' + profile.getEmail());
  //       alert("Logged In Successfully ");
            
  //      /* Write Your Code Here */
    
  //     }, (error:any) => {
  //       alert(JSON.stringify(error, undefined, 2));
  //       console.log(error);
  //     });
 
  // }
  
  // /**
  //  * Write code on Method
  //  *
  //  * @return response()
  //  */
  // googleAuthSDK() {
     
  //   (<any>window)['googleSDKLoaded'] = () => {
  //     (<any>window)['gapi'].load('auth2', () => {
  //       this.auth2 = (<any>window)['gapi'].auth2.init({
  //         client_id: '284161077857-l7m2psmc9lttuca9t5ejls4fdk921quv.apps.googleusercontent.com',
  //         cookiepolicy: 'single_host_origin',
  //         scope: 'profile email',
  //         plugin_name:'QUIZZGoogleAuth'
  //       });
  //       this.callLoginButton();
  //     });
  //   }
     
  //   (function(d, s, id){
  //     var js, fjs = d.getElementsByTagName(s)[0];
  //     if (d.getElementById(id)) {return;}
  //     js = d.createElement('script'); 
  //     js.id = id;
  //     js.src = "https://apis.google.com/js/platform.js?onload=googleSDKLoaded";
  //     fjs?.parentNode?.insertBefore(js, fjs);
  //   }(document, 'script', 'google-jssdk'));
   
  // }
}
