import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RegisterComponent } from './register/register.component';
import {HttpClient,HttpClientModule}  from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule} from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbar, MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSelectModule} from '@angular/material/select';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ProfileComponent } from './profile/profile.component';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';
import { UsersidebarComponent } from './usersidebar/usersidebar.component';
import { InstructionsComponent } from './instructions/instructions.component';
import { LoadquizComponent } from './loadquiz/loadquiz.component';
import { AddcategoryComponent } from './addcategory/addcategory.component';
import { ViewcategoriesComponent } from './viewcategories/viewcategories.component';
import { AddquizComponent } from './addquiz/addquiz.component';
import { ViewquizzesComponent } from './viewquizzes/viewquizzes.component';
import { LogoutComponent } from './logout/logout.component';
import { StartComponent } from './start/start.component';
import { UpdatequizComponent } from './updatequiz/updatequiz.component';
import { ViewquestionsComponent } from './viewquestions/viewquestions.component';
import { AddquestionComponent } from './addquestion/addquestion.component';
import { NgxUiLoaderHttpModule, NgxUiLoaderModule } from 'ngx-ui-loader';
import { HomepageComponent } from './homepage/homepage.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { GoogleauthComponent } from './googleauth/googleauth.component';
import { UploadQuestionComponent } from './upload-question/upload-question.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    RegisterComponent,
    AdmindashboardComponent,
    SidebarComponent,
    ProfileComponent,
    UserdashboardComponent,
    UsersidebarComponent,
    InstructionsComponent,
    LoadquizComponent,
    AddcategoryComponent,
    ViewcategoriesComponent,
    AddquizComponent,
    ViewquizzesComponent,
    LogoutComponent,
    StartComponent,
    UpdatequizComponent,
    ViewquestionsComponent,
    AddquestionComponent,
    HomepageComponent,
    GoogleauthComponent,
    UploadQuestionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
    MatSnackBarModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatCardModule,
    MatSlideToggleModule,
    MatSelectModule,
    CKEditorModule,
    NgxUiLoaderModule,
    NgxUiLoaderHttpModule.forRoot({showForeground:true}),
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
