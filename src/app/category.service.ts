
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http:HttpClient) 
  {
    
  }
  public categories()
  {
    return this.http.get('http://localhost:8085/category/getAllCategories');
  }
  public addCategory(category: any)
  {
    return this.http.post('http://localhost:8085/category/addCategory',category);
  }
  getQuizzesOfCategory(cid:any){
    return this.http.get('http://localhost:8085/quiz/category/'+ cid);
  }
  getActiveQuizzes(){
    return this.http.get('http://localhost:8085/quiz/active');
  }
  getActiveQuizzesOfCategory(cid:any){
    return this.http.get('http://localhost:8085/quiz/category/active/'+cid);
  }
}
