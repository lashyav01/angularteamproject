import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.css']
})
export class FileuploadComponent implements OnInit {
  constructor()
  {

  }
  ngOnInit(): void
  {
    
  }
  file:any
  selectFile(event:any)
  {
    //console.log(event);
    this.file=event.target.files[0];
    console.log(this.file)
  }
  uploadFile()
  {
    let formData=new FormData();
    formData.append('file',this.file)
    alert("file uploaded successfully");
  }

}
