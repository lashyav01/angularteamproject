import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-instructions',
  templateUrl: './instructions.component.html',
  styleUrls: ['./instructions.component.css']
})
export class InstructionsComponent implements OnInit {

  qId=0;
  quiz: any;

  constructor(private route: ActivatedRoute, private service: UserService,private router:Router) {

  }

  ngOnInit() {

    this.qId = this.route.snapshot.params['qid'];
    console.log(this.qId);

    this.service.getQuiz(this.qId).subscribe((data: any) => {
      console.log(data);
      this.quiz = data;
    },
      (error) => {
        console.log(error);
        alert('Error in loading quiz data');
      });

  }

startQuiz()
{
  Swal.fire({
    title:'Do you want to start the quiz?',
    showCancelButton:true,
    confirmButtonText:`Start`,
    denyButtonText:`Don't save`,
    icon:'info',
  }).then( (result) =>
  {
    if(result.isConfirmed){
      this.router.navigate(['/start/'+this.qId]);
    }
    else if(result.isDenied){
      Swal.fire('','','info');
    }
  });
}

}
