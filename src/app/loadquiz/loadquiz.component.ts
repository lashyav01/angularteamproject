import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-loadquiz',
  templateUrl: './loadquiz.component.html',
  styleUrls: ['./loadquiz.component.css']
})
export class LoadquizComponent implements OnInit {
 
  quizzes: any;
  catId: any;

  constructor(private route: ActivatedRoute, private service: UserService) { }

  ngOnInit() {

    this.catId = this.route.snapshot.params['catId'];
    this.route.params.subscribe((params) => { console.log(params); })

    if (this.catId == 0) {
      console.log("load all quizzes");
      this.service.viewquizzes().subscribe((data: any) => {
        this.quizzes = data;
        console.log(data);
      },
        (error) => {
          Swal.fire('error', 'loading quiz', 'error');
        });
    }
    else {
      this.service.getQuizzesOfCategory(1).subscribe((data: any) => {
        this.quizzes = data;
        console.log(data);
      },(error:any)=>{
        alert("error loading quiz");
      }
      );

    }

  }

}
