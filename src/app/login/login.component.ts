import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData: any;
  constructor(private service: UserService, private snack: MatSnackBar, private router: Router) {

  }

  ngOnInit() { }

  async login(LoginForm: any) {
    console.log(LoginForm);

    if (LoginForm.username === 'admin' && LoginForm.password === 'admin') {
      // alert('Welcome to Admin Home Page');
      this.service.setUserLoggedIn();
      this.router.navigate(['admin/profile']);
    }
    else {
      await this.service.getUser(LoginForm).then((userData: any) => {
        this.loginData = userData;

        //console.log(userData);

        if (userData != null) {
          // alert('Login success');
          this.service.setUserLoggedIn();
          this.router.navigate(['userdashboard']);
        }

        else {
          this.snack.open('Invalid credentials ', '', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
          });
        }
      });
    }
  }

}
