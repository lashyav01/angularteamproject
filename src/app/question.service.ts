import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private http:HttpClient) { }
  public getQuestionsOfQuiz(qid:any)
  {
    return this.http.get('http://localhost:8085/question/getQuestionsOfQuiz/'+qid);
  }
  public addQuestion(question:any)
  {
    return this.http.post('http://localhost:8085/question/addQuestion',question);
  }
  public deleteQuestion(qid:any)
  {
    return this.http.delete('http://localhost:8085/question/'+qid);
  }
}
