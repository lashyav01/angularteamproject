import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(private http:HttpClient) 
  { 

  }
  public quizzes()
  {
    return this.http.get('http://localhost:8085/quiz/getAllQuizzes');
  }
  public addQuiz(quiz:any)
  {
    return this.http.post('http://localhost:8085/quiz/addQuiz',quiz);
  }
  public deleteQuizzes(qid:any)
  {
    return this.http.delete('http://localhost:8085/quiz/deleteQuizzes/'+qid);
  }
  //get the single quiz
  public getQuiz(qId:any)
  {
    return this.http.get('http://localhost:8085/quiz/getQuizzes/'+qId);
  }
  public updateQuiz(quiz:any)
  {
    return this.http.put('http://localhost:8085/quiz/updateQuizzes',quiz);
  }
}
