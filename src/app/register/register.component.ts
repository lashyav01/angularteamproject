import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: any;

  constructor(private service: UserService, private snack: MatSnackBar, private router: Router, private fb: FormBuilder) {

    this.user = { userId: '', username: '', password: '', firstname: '', lastname: '', email: '', phone: '', enabled: 'true', };

  }
  ngOnInit() {

  }

  formSubmit(regForm: any) {

    this.user.username = regForm.username;
    this.user.password = regForm.password;
    this.user.firstname = regForm.firstname;
    this.user.lastname = regForm.lastname;
    this.user.email = regForm.email;
    this.user.phone = regForm.phone

    console.log(this.user);

    if (this.user.username == '' || this.user.username == null) {
      //alert('User is required');
      this.snack.open('Username is required!! ', '', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right',
      });
    }

    this.service.addUser(this.user).subscribe((data: any) => {

      console.log(data);
      Swal.fire('Success', 'User is registered', 'success');
      // this.router.navigate(['login']);
    },
      (error: any) => {
        Swal.fire('OOPS.!', 'Username already exists..Try another?', 'error');
      });

  }
  
}
