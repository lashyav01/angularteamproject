import { Component, HostListener, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { LocationStrategy } from '@angular/common';
import { Subscription } from 'rxjs';
import { jsPDF } from "jspdf";

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {

  qid: any;
  questions: any;
  currentQuestionNo: number = 0;
  timer: any;
  subscription: Subscription[] = [];

  marksGot = 0;
  correctAns = 0;
  attempted = 0;

  isSubmit = false;
  msg: string = "";
  quiz:any;

  @ViewChild('marks', { static: false }) el!: ElementRef;

  constructor(private service: UserService,
    private route: ActivatedRoute,
    private locationSt: LocationStrategy) { }

  ngOnInit() {

    this.preventBackButton();
    this.qid = this.route.snapshot.params['qid'];
    console.log(this.qid);
    this.loadQuestions();

    document.addEventListener('visibilitychange', () => {
      document.title = document.visibilityState;
      console.log(document.visibilityState);
      console.log(document.hidden);

      if (document.hidden) {

        this.evalQuiz();
        //  this.tabSwitch();
      }
    });

  }
  // tabSwitch(){
  //   this.msg="The exam browser tab has been changed.Test got submitted..!";
  //       this.evalQuiz();
  // }

  loadQuestions() {

    this.service.getQuestionsOfQuiz(this.qid).subscribe((data: any) => {
      this.questions = data;

      this.timer = this.questions.length * 2 * 60;

      this.questions.forEach((q: any) => {
        q['givenAnswer'] = '';
      });

      console.log(this.questions);
      this.startTimer();
    },
      (error: any) => {
        console.log(error);
        Swal.fire('Error', 'Error in loading questions ofquiz');
      });

  }

  nextQuestion() {
    if (this.currentQuestionNo < this.questions.length - 1) {
      this.currentQuestionNo++;
    }
    else {
      this.subscription.forEach((element: any) => {
        element.unsubscribe();
      });
    }
  }

  prevQuestion() {
    if (this.currentQuestionNo !== 0) {
      this.currentQuestionNo--;
    }
  }

  preventBackButton() {
    history.pushState(null, '', location.href);
    this.locationSt.onPopState(() => {
      history.pushState(null, '', location.href);
    });
  }

  submitQuiz() {

    Swal.fire({
      title: 'Do you want to submit the quiz?',
      showCancelButton: true,
      confirmButtonText: `Submit`,
      icon: 'info',
    }).then((e) => {
      if (e.isConfirmed) {
        this.evalQuiz();
      }
    });
  }

  startTimer() {
    let t = window.setInterval(() => {
      if (this.timer <= 0) {
        this.evalQuiz();
        clearInterval(t);
      }
      else {
        this.timer--;
      }
    }, 1000);

  }

  getFormattedTime() {
    let mm = Math.floor(this.timer / 60);
    let ss = this.timer - mm * 60;
    return `${mm} min:${ss} sec`;
  }

  evalQuiz() {

    this.isSubmit = true;

    this.questions.forEach((q: any) => {
      if (q.givenAnswer == q.ans) {
        this.correctAns += 1;
        let marksSingle = this.questions[0].quizzes.maxMarks / this.questions.length;
        this.marksGot += marksSingle;
      }

      if (q.givenAnswer.trim() != '') {
        this.attempted++;
      }
    });
    this.marksGot = parseFloat(this.marksGot.toFixed(2));
    console.log("Attempted: " + this.attempted);
    console.log("Correct Answers:" + this.correctAns);
    console.log("MArks Got:" + this.marksGot);
    console.log(this.questions);
  }

  printPage() {

    window.print();

    // html2canvas(data).then(canvas =>{
    //   let pdf=new jsPDF('l','mm','a4');
    //   pdf.save("this.questions[currentQuestionNo].quizzes.title.pdf");
    // });
  }

  downloadPDF(){
    let pdf=new jsPDF('p','pt','a4');
    this.service.getQuiz(this.qid).subscribe((data:any)=>{ this.quiz=data; })
    pdf.html(this.el.nativeElement,{
      callback: (pdf)=>{
        pdf.save(this.quiz.title);
      }
    });
    this.service.sendPDF(pdf).subscribe((data:any) =>{ console.log(data)});
  }

  @HostListener('window:beforeunload', ['$event']) beforeUnloadHandler(event: any) {
    var isFormDirty = true;
    console.log(isFormDirty);
    if (isFormDirty)
      return false;
    else
      return true;
  }

}
