import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { UserService } from '../user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-updatequiz',
  templateUrl: './updatequiz.component.html',
  styleUrls: ['./updatequiz.component.css']
})
export class UpdatequizComponent implements OnInit{

  qId=0;
  quiz:any;
  categories:any;

  constructor(private route:ActivatedRoute,private service:UserService,private snack:MatSnackBar){

  } 
 
  ngOnInit()
  {
    this.qId=this.route.snapshot.params['qid'];
    //alert(this.qId);
    this.service.getQuiz(this.qId).subscribe((data:any)=>
    {
      this.quiz=data;
      console.log(this.quiz);
    },
    (error)=>
    {
      console.log(Error);
    });

    this.service.viewcategories().subscribe((data:any)=>
    {
      this.categories=data;
    },
    (error)=>
    {
      Swal.fire('Error!!','Error while updating the quiz','error');
    });
  }

  public updateData()
  {
    if(this.quiz.title.trim()==''||this.quiz.title==null)
    {
      this.snack.open("Title is required",'',{duration:3000});
      return;
    }

    if(this.quiz.maxMarks.trim()==''||this.quiz.maxMarks==null)
    {
      this.snack.open("please enter maximum marks",'',{duration:3000});
      return;
    }
    if(this.quiz.noOfQuestions.trim()==''||this.quiz.noOfQuestions==null)
    {
      this.snack.open("please enter number of questions",'',{duration:3000});
      return;
    }
    this.service.updateQuiz(this.quiz).subscribe((data:any)=>
    {
      Swal.fire('Success','Quiz updated','success')
    },
    (error)=>
    {
      console.log(Error);
      Swal.fire('Error','Error in updating quiz','error')
    });

  }

}
