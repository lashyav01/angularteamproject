import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-upload-question',
  templateUrl: './upload-question.component.html',
  styleUrls: ['./upload-question.component.css']
})
export class UploadQuestionComponent implements OnInit {

  file: any

  constructor(){

  }
  ngOnInit() {
    
  }

  selectFile(event: any) {
    //console.log(event);
    this.file = event.target.files[0];
    console.log(this.file)
  }
  uploadFile() {
    let formData = new FormData();
    formData.append('file', this.file)
    Swal.fire('File uploaded successfully','','success');
  }

}
