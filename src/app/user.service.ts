import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  isUserLogged: boolean;
  loginStatus: Subject<any>;

  constructor(private http: HttpClient) {
    this.isUserLogged = false;
    this.loginStatus = new Subject();
  }

  addUser(user: any) {
    return this.http.post('http://localhost:8080/register', user);
  }

  getUser(LoginForm: any): any {
    return this.http.get('http://localhost:8080/login/' + LoginForm.username + "/" + LoginForm.password).toPromise();
  }

  //LoginSuccess
  setUserLoggedIn() {
    this.isUserLogged = true;
    this.loginStatus.next(true);
  }
  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }
  setUserLogOut() {
    this.isUserLogged = false;
    this.loginStatus.next(false);
  }

  viewcategories() {
    return this.http.get('http://localhost:8080/category/getAllCategories');
  }
  addCategory(category: any) {
    return this.http.post('http://localhost:8080/category/addCategory', category);
  }

  viewquizzes() {
    return this.http.get('http://localhost:8080/quiz/getAllQuizzes');
  }
  addQuiz(quiz: any) {
    return this.http.post('http://localhost:8080/quiz/addQuiz', quiz);
  }
  //get single quiz by id
  getQuiz(qid: any) {
    return this.http.get('http://localhost:8080/quiz/getQuizzes/' + qid);
  }
  deleteQuiz(qid: any) {
    return this.http.delete('http://localhost:8080/quiz/deleteQuizzes/' + qid);
  }
  getQuizzesOfCategory(cid: any) {
    return this.http.get('http://localhost:8080/quiz/category/' + cid);
  }
  getActiveQuizzes() {
    return this.http.get('http://localhost:8080/quiz/active');
  }
  getActiveQuizzesOfCategory(cid: any) {
    return this.http.get('http://localhost:8080/quiz/category/active/' + cid);
  }
  updateQuiz(quiz: any) {
    return this.http.put('http://localhost:8080/quiz/updateQuizzes', quiz);
  }
  deleteQuizzes(qid: any) {
    return this.http.delete('http://localhost:8080/quiz/deleteQuizzes/' + qid);
  }
  getQuestionsOfQuiz(qid: any) {
    return this.http.get('http://localhost:8080/question/getQuestionsOfQuiz/' + qid);
  }
  addQuestion(question: any) {
    return this.http.post('http://localhost:8080/question/addQuestion', question);
  }
  deleteQuestion(qid: any) {
    return this.http.delete('http://localhost:8080/question/' + qid);
  }
  sendPDF(file:any ){
    return this.http.post('http://localhost:8080/sendPDF',file);
  }

}
