import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-userdashboard',
  templateUrl: './userdashboard.component.html',
  styleUrls: ['./userdashboard.component.css']
})
export class UserdashboardComponent implements OnInit{

  categories:any;
  quizzes:any;

  constructor(private service:UserService,private router:Router){

  }

  ngOnInit(){

    this.service.viewcategories().subscribe((data:any)=>
    {
      this.categories=data;
    });

    this.service.getActiveQuizzes().subscribe((data: any) => {
      this.quizzes = data;
      console.log(data);
    },
      (error) => {
        Swal.fire('error', 'loading quiz', 'error');
      });
}

getAllquizzes(){
  this.service.getActiveQuizzes().subscribe((data: any) => {
    this.quizzes = data;
    console.log(data);
  },
    (error) => {
      Swal.fire('error', 'loading quiz', 'error');
    });
}

getQuizOfCat(cid:any){
  this.service.getActiveQuizzesOfCategory(cid).subscribe((data: any) => {
    this.quizzes = data;
    console.log(data);
  },(error:any)=>{
    alert("error loading quiz");
  }
  );
}

}
