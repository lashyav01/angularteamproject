import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-viewallcategories',
  templateUrl: './viewallcategories.component.html',
  styleUrls: ['./viewallcategories.component.css']
})
export class ViewallcategoriesComponent implements OnInit {
  categories=[{cid:25,title:"JAVA",description:"programming"},
  {cid:25,title:"CURRENT AFFAIRS",description:"general knowledge"},
  {cid:26,title:"NUMBER SYSTEM",description:"aptitude"},
  {cid:27,title:"NUMBER SYSTEM",description:"aptitude"}];
  constructor(private category:CategoryService)
  {
   
  }
  ngOnInit(): void {
    this.category.categories().subscribe((data:any)=>{this.categories=data;console.log(this.categories)});
  }
}
