import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-viewcategories',
  templateUrl: './viewcategories.component.html',
  styleUrls: ['./viewcategories.component.css']
})
export class ViewcategoriesComponent implements OnInit {

  categories:any;

  constructor(private service:UserService,private router:Router){
    this.categories= {cid:'',title:'',description:''};
  }
  ngOnInit() {
    this.service.viewcategories().subscribe((data:any)=>{
      this.categories=data;
      console.log(this.categories);
    });
  }

  addCat(){
    this.router.navigate(['admin/addcat']);
  }

}

