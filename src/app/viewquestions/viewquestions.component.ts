import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-viewquestions',
  templateUrl: './viewquestions.component.html',
  styleUrls: ['./viewquestions.component.css']
})
export class ViewquestionsComponent implements OnInit {

  qId = 0;
  qTitle = "";
  questions = [{ quesId:"", "content": '', "option1": '', "option2": '', "option3": '', "option4": '', ans: '' }];

  constructor(private route: ActivatedRoute, private service: UserService) {

  }
  ngOnInit() {

    this.qId = this.route.snapshot.params['qid'];
    this.qTitle = this.route.snapshot.params['title'];

    this.service.getQuestionsOfQuiz(this.qId).subscribe((data: any) => {
      console.log(data);
      this.questions = data;
    },
      (Error) => {
        Swal.fire('Error', 'error in', 'error');
      });

  }
  deleteQuestion(qid: any) {
    Swal.fire(
      {
        icon: "info",
        title: "are you sure?",
        confirmButtonText: "Yes",
        showCancelButton: true
      }
    ).then((result) => {
      if (result.isConfirmed) {
        this.service.deleteQuestion(qid).subscribe((data: any) => {
          this.questions = this.questions.filter((quiz: any) => quiz.quesId != qid);
          Swal.fire('Success', 'Question Deleted', 'success')
        },
          (Error) => {
            Swal.fire('Error', 'Unable to Delete Quiz', 'error')
          });
      }

    });
  }
  

}

