import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewquizzesComponent } from './viewquizzes.component';

describe('ViewquizzesComponent', () => {
  let component: ViewquizzesComponent;
  let fixture: ComponentFixture<ViewquizzesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewquizzesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewquizzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
