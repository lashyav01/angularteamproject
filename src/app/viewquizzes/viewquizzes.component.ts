import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-viewquizzes',
  templateUrl: './viewquizzes.component.html',
  styleUrls: ['./viewquizzes.component.css']
})
export class ViewquizzesComponent implements OnInit {

  quizzes: any;
  quiz: any;

  constructor(private service: UserService, private router: Router) {

    this.quizzes = [
      { qid: 23, title: "BASIC JAVA QUIZ", description: 'There are a list of core java quizzes such as basics quiz, oops quiz, string handling quiz, array quiz, exception handling quiz, collection framework quiz etc.', maxMarks: '50', noOfQuestions: '20', active: '', category: { title: "Programming" } },
      { qid: 23, title: "BASIC PYTHON QUIZ", description: '', maxMarks: '50', noOfQuestions: '20', active: '', category: { title: "Programming" } }
    ];

  }

  ngOnInit(): void {
    this.service.viewquizzes().subscribe((data: any) => {
      this.quizzes = data;
      console.log(this.quizzes);
    });
  }

  addQuizClick() {
    this.router.navigate(['admin/addquiz']);
  }
  deleteQuizClick(qid: any) {

    Swal.fire(
      {
        icon: "info",
        title: "Are you sure?",
        confirmButtonText: "Yes",
        showCancelButton: true
      }
    ).then((result) => {
      if (result.isConfirmed) {
        this.service.deleteQuiz(qid).subscribe((data: any) => {
          this.quizzes = this.quizzes.filter((quiz: any) => quiz.qid != qid);
          Swal.fire('Success', 'Quiz Deleted', 'success')
        },
          (Error) => {
            Swal.fire('Error', 'Unable to Delete Quiz', 'error')
          });
      }

    })
  }
}
